# github-grapher
`github-grapher` is a tool that will print some image in your Github contributions

## How it works
When `script.py` is run, it reads `art.txt`, and fetches a character according to these rules: the column is the week number minus the value of `FIRST_WEEK` in the script, and the row is the weekday, with sunday on the first row and saturday on the bottom row.
Then, it matches this character against a "palette" which maps characters to a number of commits to create.
Lastly, it loops that many times and writes a string consisting of the date and which commit number it is into a file, adds with git and commits with the same message as the file contents.

### Disclaimer
**Note that Github saves your statistics separately, so if you do commits, you can't reset the statistics. You can't reset the number of commits to 0 for a day**

## How to run
You can run this yourself by following these steps:

1. Make sure you have a disposable repository for your Github account to push these commits to. Important things to note are:
    - You must have the same e-mail for the Github account as you make the commits with. You can set this with `git config user.email "your_email@abc.com"` and confirm it with `git config user.email`.
    - The repository must be standalone (not a fork).
    - The repository must be public. Only people who can see the repository and its commits can see the contributions in your graph.
    - You must be on the default branch (commonly master or main).
    - Make sure that you have SSH authentication set up! This script does not enter passwords for you when pushing the commits.
2. Clone this repository onto a 24/7 running server
3. Move the example `config.json` and `art.txt` to the target repository, or create new ones.
4. Adjust `config.json` to your needs. You might need to adjust
    - Set `ascii art file` to the ASCII image relative to the target repository's root. Note that each character will be represented as a pixel in your graph. The easiest is to just paste whatever ASCII art on 7 lines into here.
    - Set `palette` to include the characters in your `ascii art file` and their corresponding number of commits. Choose small numbers that still are like a factor of 10 larger than what you commit in a day.
    - Set `first week` to the week you want to be the first of the image in you github graph. This must be at the earlist the current, since we can't create old commits.
5. Add the script to your crontab. My crontab line for this script is run like
```
0 1 * * * /home/z-nexx/github-grapher/script.py /home/z-nexx/githubgraph-test/
```
Where `/home/z-nexx/github-grapher/` is where I forked this repo to, and /home/z-nexx/githubgraph-test/ is the repository that will hold the commits. The script is run at 01:00 every day.

## Cleaning up the disposable repository
If you want to clean up your commits because you were messing around, just:
- Go to the repository and run `git update-ref -d HEAD`. This will remove all commits. The scripts forcefully pushes, so it should update the commits properly upstream too.
- Empty `log.txt`

## Contributions

Thanks to `shoe` for the idea! May his Github profile forever be beautiful!
