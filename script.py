#!/usr/bin/python3
import json
import datetime
import time
import os
import sys

def read_config(config_file_name):
    with open(config_file_name, 'r') as cf:
        configtext = cf.read()
        return json.loads(configtext)

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print(f'Repository path omitted!')
        print(f'usage: {sys.argv[0]} REPOSITORY_PATH')
        print(f'quitting')
        quit()

    # go to script dir
    os.chdir(sys.argv[1])

    config = read_config("config.json")

    with open(config["ascii art file"]) as f, open(config["log file"], "a") as logfile:
        lines = f.read().splitlines()

        if len(lines) != 7:
            print(f'{config["ascii art file"]} has {len(lines)} lines! It needs to have 7')
            quit()

        current_date = datetime.date.today()

        row  = current_date.isoweekday() % 7 # sunday = 0, saturday = 6
        column = 0

        first_year = config["year of first week"]
        first_week = config["first week"]
        sunday_of_first_week = datetime.datetime.strptime(f'{first_year}-W{first_week}-1', "%Y-W%W-%w").date() - datetime.timedelta(days=1)

        if current_date < sunday_of_first_week:
            print(f'Week {config["first week"]} hasn\'t been yet - quitting')
            quit()

        time_since_first_sunday = current_date - sunday_of_first_week
        weeks_since_first_week = int(abs(time_since_first_sunday.days) / 7)

        column = weeks_since_first_week

        if config["repeat"]:
            column = column % len(lines[row]) # wrap the column if repeating
        elif column >= len(lines[row]):
            last_week_str = (sunday_of_first_week + datetime.timedelta(weeks=len(lines[row]))).strftime("%Y-W%U")
            print(f'{config["ascii art file"]} startedat {first_year}-W{first_week} ({weeks_since_first_week} weeks ago), but only has data until {last_week_str} ({len(lines[row])} weeks)')
            quit()

        character = lines[row][column]
        try:
            amount = config["palette"][character]
        except KeyError:
            amount = 0

        print(f'{current_date}: doing {amount} commits')

        if amount == 0:
            timestamp = datetime.datetime.now().strftime(config["log timestamp format"])
            logfile.write(f'{timestamp}: 0 commits\n')

        for n in range(amount):
            line = f'{current_date} {n + 1} of {amount}'

            with open(config["file in repository"], "w") as target:
                target.write(line)

            os.system(f'git add {config["file in repository"]}')
            os.system(f'git commit -m \"{line}\"')

            timestamp = datetime.datetime.now().strftime(config["log timestamp format"])
            logfile.write(f'{timestamp}: {line}\n')
            time.sleep(config["seconds between commits"])
        os.system(f'git push --force')
